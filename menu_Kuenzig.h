
// file is included in more than one place
#define WIN32_LEAN_AND_MEAN
#ifndef MENU_H // Prevent multiple definitions if this
#define MENU_H

class Menu;

#include "graphics.h"
#include "constants.h"
#include "textDX.h"
#include "image.h"
#include "textureManager.h"
#include <string>
#include <sstream>
#include <vector>
#include "input.h"

namespace menuNS
{
}

struct menuItem
{
    std::string name;
    Menu *subMenu;
};

// inherits from Entity class
class Menu
{
  private:
    TextDX *menuItemFont;
    TextDX *menuItemFontHighlight;
    TextDX *menuHeadingFont;
    Input *input; // pointer to the input system
    Graphics *graphics;
    int selectedItem;
    std::string menuHeading;
    std::vector<menuItem> menuItems;
    D3DXVECTOR2 menuAnchor;
    int verticalOffset;
    int horizontalOffset;
    int linePtr;
    COLOR_ARGB highlightColor;
    COLOR_ARGB normalColor;
    TextureManager menuBoxTexture;
    std::vector<Image> menuBoxImages;

  public:
    // Constructor
    Menu(void);
    // Destructor
    ~Menu(void);
    void initialize(Graphics *g, Input *i, std::string heading, std::vector<menuItem> items);
    void update();
    int getSelectedItem();
    void displayMenu();
#define MENU_BACKGROUND_SCALE 1.0f
#define MENU_BOX_SCALE 1.5f
};
#endif
