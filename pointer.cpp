// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 version 1.0

#include "pointer.h"

//=============================================================================
// default constructor
//=============================================================================
Pointer::Pointer() : Entity()
{
    spriteData.x    = pointerNS::X;              // location on screen
    spriteData.y    = pointerNS::Y;
	spriteData.width = (int)(pointerNS::WIDTH * pointerNS::POINTER_IMAGE_SCALE);           // size of Ship1
	spriteData.height = (int)(pointerNS::HEIGHT * pointerNS::POINTER_IMAGE_SCALE);
    radius          = pointerNS::COLLISION_RADIUS;
    mass            = pointerNS::MASS;
	collisionType = entityNS::BOX;
	edge.bottom = pointerNS::HEIGHT/2;
	edge.top = -pointerNS::HEIGHT/2;
	edge.left = -pointerNS::WIDTH/2;
	edge.right = pointerNS::WIDTH/2;
	myTimer = 0;
	velDir = 1;
}

void Pointer::update(float frameTime) {
	Entity::update(frameTime);

	myTimer+= frameTime;
	spriteData.x += 50 * frameTime * velDir;

	if (myTimer > .6)
	{
		myTimer = 0;
		velDir *= -1;
	}
}