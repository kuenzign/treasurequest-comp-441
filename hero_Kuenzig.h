#ifndef _HERO_H
#define _HERO_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "input.h"
#include "graphics.h"
#include "game.h"
#include "GeneralEntity_Kuenzig.h"
#include "weapon.h"

namespace heroNS
{
	/*Where we spawn our heroes*/
	const int X = 10;   // location on screen
	const int Y = 10;
	const float SPEED = 140;                // 200 pixels per second
}

class Hero : public GeneralEntity
{
public:
	Hero(void);

	void setWeapon(Game *gamePtr, int width, int height, int ncols, TextureManager *textureM){
		ofChoice.initialize(gamePtr, width, height, ncols, textureM);
	}

	// inherited member functions
	virtual void update(float frameTime);

	void trackSpot(float x, float y, float frameTime);

	VECTOR2 getVelocity() {return velocity;}

private:
	weapon ofChoice;
};

#endif