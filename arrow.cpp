#include "arrow.h"

arrow::arrow(void)
{
    collisionType = entityNS::BOX;
    clickX = 0;
    clickY = 0;
    moveArrow = false;
    edge.left = -arrowNS::WIDTH / 8;
    edge.bottom = arrowNS::HEIGHT / 4;
    edge.right = arrowNS::WIDTH / 8;
    edge.top = -arrowNS::HEIGHT / 4;
    timeCount = 0;
}

arrow::~arrow(void)
{
}

void arrow::update(float frameTime)
{
    weapon::update(frameTime);

    if (input->getMouseRButton() && !moveArrow)
    {
	input->setMouseRButton(false);

	//audio->playCue(BEEP1);

	clickX = input->getMouseX() - getCenterX();
	clickY = input->getMouseY() - getCenterY();
	setRadians((float)atan2(clickY, clickX));

	/*clock_t start_time = clock();
		clock_t end_time = 1000 + start_time;
		while(clock() != end_time);*/

	velocity.x = arrowNS::SPEED;
	velocity.y = arrowNS::SPEED;
	squareCalc = sqrt((clickX * clickX) + (clickY * clickY));
	shot = D3DXVECTOR2(clickX / squareCalc, clickY / squareCalc);
	audio->playCue(SHOT_SONG);
	moveArrow = true;
    }
    /////////////////////////////////////Check for mouse click

    /////////////////////////////////////Check if arrow left boundaries
    if (spriteData.x > GAME_WIDTH || spriteData.x + spriteData.width * spriteData.scale < 0 || spriteData.y > GAME_HEIGHT || spriteData.y + spriteData.height * spriteData.scale < 0)
    {
	moveArrow = false;
	timeCount = 0;
    }
    /////////////////////////////////Check if arrow left boundaries

    //////////////////////////////////////Move Arrow if true
    if (moveArrow)
    {
	timeCount += frameTime;
	if (timeCount > (ARCHER_SHOOTING_END-ARCHER_SHOOTING_START)*GENERAL_ANIMATION_DELAY)
	{
	    setVisible(true);
	    setActive(true);
	    spriteData.x += shot.x * velocity.x * frameTime;
	    spriteData.y += shot.y * velocity.y * frameTime;
	}
    }
    else
    {
	setVisible(false);
	setActive(false);
    }
    /////////////////////////////////////End Move Arrow
}

bool arrow::initialize(Game *gamePtr, int width, int height, int ncols, TextureManager *textureM)
{
    return (Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void arrow::moveOffScreen()
{
    velocity.x = 10000;
}