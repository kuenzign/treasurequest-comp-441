// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 version 1.0

#include "Logo.h"

//=============================================================================
// default constructor
//=============================================================================
Logo::Logo() : Entity()
{
    spriteData.x    = logoNS::X;              // location on screen
    spriteData.y    = logoNS::Y;
	spriteData.width = (int)(logoNS::WIDTH * logoNS::LOGO_IMAGE_SCALE);         
	spriteData.height = (int)(logoNS::HEIGHT * logoNS::LOGO_IMAGE_SCALE);
}