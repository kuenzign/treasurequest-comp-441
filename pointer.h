// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 planet.h v1.0

#ifndef _POINTER_H              // Prevent multiple definitions if this 
#define _POINTER_H              // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace pointerNS
{
    const int   WIDTH = 1000;                // image width
    const int   HEIGHT = 803;               // image height
	const float   POINTER_IMAGE_SCALE = 0.1f;
    const int   COLLISION_RADIUS = 120/2;   // for circular collision
    const int   X = GAME_WIDTH/2 - WIDTH/2; // location on screen
    const int   Y = GAME_HEIGHT/2 - HEIGHT/2;
    const float MASS = 1.0e14f;         // mass
}

class Pointer : public Entity            // inherits from Entity class
{
public:
    // constructor
    Pointer();

	void update(float frameTime);
private:
	float myTimer;
	int velDir;
};
#endif