#include "Slime_Kuenzig.h"


Slime::Slime(void) : Enemy()
{
	invincible = true;
	timer = 0.0f;
	switchTime = RAND_FLOAT(0.5f, 3.0f);
}

void Slime::doInvincible(){
	setFrames(SLIME_INVINCIBLE_START*GENERAL_NUM_DIRECTIONS+direc, SLIME_INVINCIBLE_END*GENERAL_NUM_DIRECTIONS+direc);
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Slime::update(float frameTime, Hero target, bool &collide)
{
	timer += frameTime;
	if(timer >= switchTime){
		invincible = !invincible;
		timer = 0.0f;
		switchTime = RAND_FLOAT(0.5f, 3.0f);
		if(invincible){
			audio->playCue(SLIME_DOWN);
		}else{
			audio->playCue(SLIME_UP);
		}
	}
	Enemy::update(frameTime, target, collide);
}

