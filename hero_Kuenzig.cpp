#include "hero_Kuenzig.h"

//=============================================================================
// default constructor
//=============================================================================
Hero::Hero(): GeneralEntity(){
	direc = upleft;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Hero::update(float frameTime)
{
	GeneralEntity::update(frameTime);

	if(input->getMouseLButton()){//Walk
		targetX = (float)input->getMouseX();
		targetY = (float)input->getMouseY();
		walking = true;
	}

	if (walking){
		attacking = false;
		trackSpot(targetX,targetY, frameTime);
	}

	direction temp = direc;
	if(getPosX() < input->getMouseX())
	{
		if(getPosY() < input->getMouseY())
		{
			direc = downright;
		}
		else
		{
			direc = upright;
		}
	}		
	else
	{
		if(getPosY() < input->getMouseY())
		{
			direc = downleft;
		}
		else
		{
			direc = upleft;
		}
	}
	if(temp != direc){
		if(velocity.x != 0 || velocity.y != 0 && !attacking){
			setCurrentFrame(GENERAL_WALKING_START*GENERAL_NUM_DIRECTIONS+direc);
		}
		else if(attacking){
			setCurrentFrame(ARCHER_SHOOTING_START*GENERAL_NUM_DIRECTIONS+direc);
		}
		else{
			setCurrentFrame(GENERAL_RESTING_START*GENERAL_NUM_DIRECTIONS+direc);
		}
	}
}

void Hero::trackSpot(float x, float y, float frameTime){
	convertScreenToGrid(x, y);

	VECTOR2 vel = VECTOR2(getPosX(), getPosY()) - VECTOR2(x, y);
	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(-vel);

	spriteData.x += frameTime * velocity.x * heroNS::SPEED;
	spriteData.y += frameTime * velocity.y * heroNS::SPEED;

	if ((getPosX() >= x - 1 && getPosX() <= x + 1) && (getPosY() >= y - 1 && getPosY() <= y + 1)){
		walking = false;
		setVelocity(VECTOR2(0, 0));
	}
}