#include "TreasureQuest.h"
#include "particleManager.h"
#include <cmath>

ParticleManager pm;

//=============================================================================
// Constructor
//=============================================================================
TreasureQuest::TreasureQuest()
{
	AddFontResourceEx("fonts/8bitlim.ttf", FR_PRIVATE, 0);
	AddFontResourceEx("fonts/BitDarling10(sRB).ttf", FR_PRIVATE, 0);

	dxFontSmall = new TextDX();
	background_and_ready_game = false;
	countDownOn = false;
	roundOver = false;
	tutorialText = new TextDX();
	stateTimer = 0;
	dead = false;
	killCount = 0;
	firstClick = false;

	lsplash = 0.0;
	l1Timer = 0.0;
	l2Timer = 0.0;
	l3Timer = 0.0;
	lvlTime = 0.0;
	totalTime = 0.0;
	contactMade = false;
	healthTimer = 0.0;
	setSkelTime = false;
	patternCalled = false;
	luisTimer = 0.0;
	setLTimer = false;
	up1 = false;
	aiTime = 0.0;
	for (int i = 0; i < 20; i++)
	{
		treeVals[i] = (float)(i * 50 + 50);
	}
	positions1[0] = treeVals[11];
	positions1[1] = treeVals[1];
	positions1[2] = treeVals[18];
	positions1[3] = treeVals[5];
	positions1[4] = treeVals[3];
	positions1[5] = treeVals[6];

	positions2[0] = treeVals[5];
	positions2[1] = treeVals[2];
	positions2[2] = treeVals[12];
	positions2[3] = treeVals[6];
	positions2[4] = treeVals[18];
	positions2[5] = treeVals[1];
}

//=============================================================================
// Destructor
//=============================================================================
TreasureQuest::~TreasureQuest()
{
	releaseAll(); // call onLostDevice() for every graphics item
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void TreasureQuest::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError

	pm.initialize(graphics);

	if (dxFontSmall->initialize(graphics, 20, true, false, "Bit Darling10 (sRB)") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	score = 0;

	timeInState = 0;
	gameStates = menu;

	mainMenu = new Menu();
	subMenu = new Menu();
	std::vector<menuItem> mainItems;
	std::vector<menuItem> subItems;

	menuItem subItem1 = {"Play", nullptr};
	subItems.push_back(subItem1);
	menuItem subItem2 = {"Tutorial", nullptr};
	subItems.push_back(subItem2);
	subMenu->initialize(graphics, input, "The Quest Has Just Begun!", subItems);

	menuItem mainItem1 = {"Play Now", subMenu};
	mainItems.push_back(mainItem1);
	mainMenu->initialize(graphics, input, "Welcome to TreasureQuest!", mainItems);

	//tutorial stuff
	if (tutorialText->initialize(graphics, 40, true, false, "Bit Darling10 (sRB)") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	playMusic = true;
	playOtherMusic = true;

	if (!healthTexture.initialize(graphics, HEALTH_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Health texture initialization failed"));

	//Initializing the archer
	if (!archerTexture.initialize(graphics, ARCHER_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Archer texture initialization failed"));
	if (!Luis.initialize(this, &archerTexture, &healthTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init archer"));
	Luis.setLoop(true);

	//Init Slime
	if (!slimeTexture.initialize(graphics, SLIME_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Slime texture initialization failed"));

	for (int i = 0; i < 3; i++)
	{
		if (!slimeArr[i].initialize(this, &slimeTexture, &healthTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init slimes"));
		slimeArr[i].setX(-1000);
		slimeArr[i].setY(0);
		slimeArr[i].setFrameDelay(SLIME_ANIMATION_DELAY);
		slimeCollide[i] = false;
		slimesSet[i] = false;
	}

	//Init Skeleton texture
	if (!skelTexture.initialize(graphics, SKELETON_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Skeleton texture initialization failed"));

	for (int i = 0; i < 3; i++)
	{
		if (!skelArr[i].initialize(this, &skelTexture, &healthTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init Skeletons"));
		skelArr[i].setX(-1000);
		skelArr[i].setY(0);
		skelCollide[i] = false;
		skelsSet[i] = false;
	}

	//Init orc texture
	if (!orcTexture.initialize(graphics, ORC_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Orc texture initialization failed"));

	//These orcs are initially set off screen
	for (int i = 0; i < 3; i++)
	{
		if (!orcArr[i].initialize(this, &orcTexture, &healthTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init orcs"));
		orcArr[i].setX(-1000);
		orcArr[i].setY(0);
		orcCollide[i];
		orcsSet[i] = false;
	}

	/*for (int i = 0; i < 6; i++){
	if (i < 3)
	aiManagerArr[i] = new aiManager_Venegas(orcArr[i]);
	else
	aiManagerArr[i] = new aiManager_Venegas(skelArr[i-3]);
	}*/

	if (!arrowTexture.initialize(graphics, ARROW_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Arrrow texture initialization failed"));
	if (myArrow.initialize(this, arrowNS::WIDTH, arrowNS::HEIGHT, 1, &arrowTexture) == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init arrow"));
	myArrow.setX(Luis.getX() + GENERAL_WIDTH);
	myArrow.setY((Luis.getY() + (GENERAL_HEIGHT) / 3 + 10));
	myArrow.setScale(ARROW_IMAGE_SCALE);

	if (!map1Texture.initialize(graphics, MAP1_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Map 1 texture initialization failed"));
	for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
	{
		if (!map1[i].initialize(graphics, 0, 0, 0, &map1Texture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init grass"));
		map1[i].setScale(MAP1_IMAGE_SCALE);
		map1[i].setX(map1[i].getWidth() * map1[i].getScale() * (i % MAP_IMAGE_COLUMNS));
		map1[i].setY(map1[i].getHeight() * map1[i].getScale() * (i / MAP_IMAGE_ROWS));
	}

	if (!map1SelTexture.initialize(graphics, MAP1_SEL_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Map 1 selected texture initialization failed"));
	if (!map1Sel.initialize(graphics, 0, 0, 0, &map1SelTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init grass selection"));
	map1Sel.setScale(MAP1_IMAGE_SCALE);
	map1Sel.setX(0);
	map1Sel.setY(0);
	map1Sel.setVisible(false);

	if (!map2Texture.initialize(graphics, MAP2_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Map 2 texture initialization failed"));
	for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
	{
		if (!map2[i].initialize(graphics, 0, 0, 0, &map2Texture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init dirt"));
		map2[i].setScale(MAP2_IMAGE_SCALE);
		map2[i].setX(map2[i].getWidth() * map2[i].getScale() * (i % MAP_IMAGE_COLUMNS));
		map2[i].setY(map2[i].getHeight() * map2[i].getScale() * (i / MAP_IMAGE_ROWS));
	}
	if (!map2SelTexture.initialize(graphics, MAP2_SEL_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Map 2 selected texture initialization failed"));
	if (!map2Sel.initialize(graphics, 0, 0, 0, &map2SelTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init dirt selection"));
	map2Sel.setScale(MAP2_IMAGE_SCALE);
	map2Sel.setX(0);
	map2Sel.setY(0);
	map2Sel.setVisible(false);

	if (!map3Texture.initialize(graphics, MAP3_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Map 3 texture initialization failed"));
	for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
	{
		if (!map3[i].initialize(graphics, 0, 0, 0, &map3Texture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init stone"));
		map3[i].setScale(MAP3_IMAGE_SCALE);
		map3[i].setX(map3[i].getWidth() * map3[i].getScale() * (i % MAP_IMAGE_COLUMNS));
		map3[i].setY(map3[i].getHeight() * map3[i].getScale() * (i / MAP_IMAGE_ROWS));
	}

	if (!map3SelTexture.initialize(graphics, MAP3_SEL_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Map 3 selected texture initialization failed"));
	if (!map3Sel.initialize(graphics, 0, 0, 0, &map3SelTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init stone selection"));
	map3Sel.setScale(MAP3_IMAGE_SCALE);
	map3Sel.setX(0);
	map3Sel.setY(0);
	map3Sel.setVisible(false);

	//Menus:
	if (!menuTexture.initialize(graphics, ALT_MENU))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Menu 1 texture initialization failed"));
	if (!menuImage.initialize(graphics, 0, 0, 0, &menuTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init menu 1"));
	menuImage.setX(GAME_WIDTH / 2 - (menuImage.getWidth() * MENU_BACKGROUND_SCALE) / 2);
	menuImage.setY(GAME_HEIGHT / 2 - (menuImage.getHeight() * MENU_BACKGROUND_SCALE) / 2);
	menuImage.setScale(MENU_BACKGROUND_SCALE);

	if (!logoTexture.initialize(graphics, LOGO_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Logo texture initialization failed"));
	if (!logo.initialize(this, 0, 0, 0, &logoTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init logo"));
	logo.setX(180);
	logo.setY(60);
	logo.setScale(LOGO_IMAGE_SCALE);

	/*if (!boxLightTexture.initialize(graphics, CHARACTER_BOX_IMAGE))
	throw(GameError(gameErrorNS::FATAL_ERROR, "Box texture initialization failed"));
	for(int i = 0; i < 3; i++){
	if (!boxLight[i].initialize(this, 320, 320, 0, &boxLightTexture))
	throw(GameError(gameErrorNS::FATAL_ERROR, "Error init box outlines"));
	boxLight[i].setScale(CHARACTER_BOX_IMAGE_SCALE);
	boxLight[i].setX((float)(110 + 400*i));
	boxLight[i].setY((float)250);
	}

	if (!charBoxTexture.initialize(graphics, CHARACTER_BACKGROUND_IMAGE))
	throw(GameError(gameErrorNS::FATAL_ERROR, "Character texture initialization failed"));
	for(int i = 0; i < 3; i++){
	if (!charBoxes[i].initialize(this, 300, 300, 0, &charBoxTexture))
	throw(GameError(gameErrorNS::FATAL_ERROR, "Error init character boxes"));
	charBoxes[i].setScale(CHARACTER_BACKGROUND_SCALE);
	charBoxes[i].setX((float)(118 + 400*i));
	charBoxes[i].setY(258);
	}

	if (!archerSelectTexture.initialize(graphics, ARCHER_BACKGROUND_IMAGE))
	throw(GameError(gameErrorNS::FATAL_ERROR, "Archer select texture initialization failed"));
	if (!archerSelect.initialize(this, 450, 520, 0, &archerSelectTexture))
	throw(GameError(gameErrorNS::FATAL_ERROR, "Error init archer select texture"));
	archerSelect.setX(128);
	archerSelect.setY(250);
	archerSelect.setScale(CHARACTER_SELECT_SCALE);*/

	if (!smallTreesTexture.initialize(graphics, SMALL_TREES))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Small trees texture initialization failed"));
	for (int i = 0; i < 3; i++)
	{
		if (!smallTrees[i].initialize(this, 82, 144, 0, &smallTreesTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init small trees"));
		smallTrees[i].setScale(SMALL_TREES_SCALE);
		smallTrees[i].setCollisionType(entityNS::BOX);
		smallTrees[i].setEdge(TreasureQuestNS::SMALL_RECTANGLE);
	}

	smallTrees[0].setX(positions1[0]);
	smallTrees[0].setY(positions1[1]);

	smallTrees[1].setX(positions1[2]);
	smallTrees[1].setY(positions1[3]);

	smallTrees[2].setX(positions1[4]);
	smallTrees[2].setY(positions1[5]);

	if (!snowTreesTexture.initialize(graphics, SNOW_TREES))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Snow trees texture initialization failed"));
	for (int i = 0; i < 3; i++)
	{
		if (!snowTrees[i].initialize(this, 131, 225, 0, &snowTreesTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init snow trees"));
		snowTrees[i].setScale(SNOW_TREES_SCALE);
		snowTrees[i].setCollisionType(entityNS::BOX);
		snowTrees[i].setEdge(TreasureQuestNS::SNOW_RECTANGLE);
	}

	snowTrees[0].setX(positions2[0]);
	snowTrees[0].setY(positions2[1]);

	snowTrees[1].setX(positions2[2]);
	snowTrees[1].setY(positions2[3]);

	snowTrees[2].setX(positions2[4]);
	snowTrees[2].setY(positions2[5]);

	for (int j = 0; j < 9; j++)
	{
		patternStepIndex = 0;
		for (int i = 0; i < maxPatternSteps; i++)
		{
			if (j < 3)
				steps[j][i].initialize(&orcArr[j]);
			else if (j < 6)
				steps[j][i].initialize(&skelArr[j - 3]);
			else
				steps[j][i].initialize(&slimeArr[j - 6]);

			steps[j][i].setActive();
		}
	}
	setPatterns();
	resetPattern = false;
	return;
}

//=============================================================================
// Update all game items
//=============================================================================

void TreasureQuest::gameStateUpdate()
{
	timeInState += frameTime;

	if (gameStates == tutorial)
	{
		stateTimer += frameTime;
		if (stateTimer > 10)
		{
			gameStates = menu;
			stateTimer = 0;
		}
	}

	if (gameStates == level1)
	{
		aiTime += frameTime;

		if (!patternCalled)
		{
			setPatterns();
			patternCalled = true;
		}

		for (int i = 0; i < 3; i++)
		{
			if ((abs(Luis.getPosX() - orcArr[i].getPosX())) + (abs(Luis.getPosX() - orcArr[i].getPosX())) < 400)
			{
				if (orcArr[i].isAlive())
				{
					setTrack();
				}
			}
			else if (aiTime > 4)
			{
				setPatterns();
				aiTime = 0;
			}
		}

		smallTrees[0].setX(positions1[0]);
		smallTrees[0].setY(positions1[1]);

		smallTrees[1].setX(positions1[2]);
		smallTrees[1].setY(positions1[3]);

		smallTrees[2].setX(positions1[4]);
		smallTrees[2].setY(positions1[5]);

		for (int i = 0; i < 3; i++)
		{
			snowTrees[i].setX(-2000);
			snowTrees[i].setY(0);
		}

		if (input->isKeyDown(VK_SPACE))
		{
			for (int i = 0; i < 3; i++)
			{
				orcArr[i].setInvisible();
			}
		}

		lvlTime += frameTime;
		if (input->isKeyDown(VK_RIGHT) && input->isKeyDown(VK_UP) && input->isKeyDown(VK_LEFT))
		{
			lvlTime = 0;
			killCount += 3;
			gameStates = level2;
			Luis.setX(GAME_WIDTH / 32);
			Luis.setY(GAME_HEIGHT / 32);
			firstClick = true;
			for (int i = 0; i < 3; i++)
			{
				orcArr[i].setX(-1000);
				orcArr[i].setY(0);
			}
		}

		if (lvlTime > 0.5)
		{
			if (input->getMouseLButton() == true || input->getMouseRButton() == true)
			{
				firstClick = false;
			}
		}

		timeInState = 0;
		for (int i = 0; i < 3; i++)
		{
			if (!orcsSet[i])
			{
				orcArr[i].setX((float)GAME_WIDTH - (i * 300) - 230);
				orcArr[i].setY((float)i * 180);
				orcsSet[i] = true;
			}
		}

		if (killCount > 2)
		{
			gameStates = l1complete;
			Luis.setX(GAME_WIDTH / 32);
			Luis.setY(GAME_HEIGHT / 32);
			firstClick = true;
			myArrow.setX(-1000);
			myArrow.setY(-2000);
			myArrow.setX(Luis.getCenterX() - arrowNS::WIDTH / 2);
			myArrow.setY((Luis.getY() + (GENERAL_HEIGHT * GENERAL_IMAGE_SCALE) / 3 + 10));
			for (int i = 0; i < 3; i++)
			{
				orcArr[i].setX(-1000);
				orcArr[i].setY(0);
				orcsSet[i] = false;
			}
		}
	}

	if (gameStates == l1complete)
	{
		resetPattern = true;
		lsplash += frameTime;
		if (lsplash > 1.5)
		{
			gameStates = level2;
			lsplash = 0;
		}
		patternCalled = false;
		Luis.resetHealth();
		aiTime = 0;
	}

	if (gameStates == level2)
	{
		aiTime += frameTime;

		for (int i = 0; i < 3; i++)
		{
			if ((abs(Luis.getCenterX() - slimeArr[i].getCenterX())) + (abs(Luis.getCenterX() - slimeArr[i].getCenterX())) < 500)
			{
				if (slimeArr[i].isAlive())
				{
					setTrack();
				}
			}
			else if (aiTime > 4)
			{
				setPatterns();
				aiTime = 0;
			}
		}

		if (!patternCalled)
		{
			setPatterns();
			patternCalled = true;
		}

		snowTrees[0].setX(positions2[0]);
		snowTrees[0].setY(positions2[1]);

		snowTrees[1].setX(positions2[2]);
		snowTrees[1].setY(positions2[3]);

		snowTrees[2].setX(positions2[4]);
		snowTrees[2].setY(positions2[5]);

		for (int i = 0; i < 3; i++)
		{
			smallTrees[i].setX(-2000);
			smallTrees[i].setY(0);
		}

		if (input->isKeyDown(VK_SPACE))
		{
			for (int i = 0; i < 3; i++)
			{
				slimeArr[i].setInvisible();
			}
		}

		lvlTime += frameTime;
		if (input->isKeyDown(VK_RIGHT))
		{
			if (input->isKeyDown(VK_UP))
			{
				if (input->isKeyDown(VK_LEFT))
				{
					lvlTime = 0;
					killCount += 3;
					gameStates = level3;
					Luis.setX(GAME_WIDTH / 32);
					Luis.setY(GAME_HEIGHT / 32);
					firstClick = true;
					for (int i = 0; i < 3; i++)
					{
						slimeArr[i].setX(-1000);
						slimeArr[i].setY(0);
					}
				}
			}
		}

		if (lvlTime > 0.5)
		{
			if (input->getMouseLButton() == true || input->getMouseRButton() == true)
			{
				firstClick = false;
			}
		}

		timeInState = 0;
		/*for (int i = 0; i < 3; i++)
		{
		if (!slimesSet[i])
		{
		slimeArr[i].setX((float)GAME_WIDTH - (i * 300) - 300);
		slimeArr[i].setY((float)i * 180);
		slimesSet[i] = true;
		}
		}*/
		if (!slimesSet[0])
		{
			{
				slimeArr[0].setX((float)GAME_WIDTH - 200);
				slimeArr[0].setY((float)240);
				slimesSet[0] = true;
			}
		}
		if (!slimesSet[1])
		{
			{
				slimeArr[1].setX((float)GAME_WIDTH - 480);
				slimeArr[1].setY((float)420);
				slimesSet[1] = true;
			}
		}
		if (!slimesSet[2])
		{
			{
				slimeArr[2].setX((float)GAME_WIDTH - 800);
				slimeArr[2].setY((float)300);
				slimesSet[2] = true;
			}
		}

		if (killCount > 5)
		{
			gameStates = l2complete;
			Luis.setX(GAME_WIDTH / 32);
			Luis.setY(GAME_HEIGHT / 32);
			firstClick = true;
			myArrow.setX(-1000);
			myArrow.setY(-2000);
			myArrow.setX(Luis.getCenterX() - arrowNS::WIDTH / 2);
			myArrow.setY((Luis.getY() + (GENERAL_HEIGHT * GENERAL_IMAGE_SCALE) / 3 + 10));
			for (int i = 0; i < 3; i++)
			{
				slimeArr[i].setX(-1000);
				slimeArr[i].setY(0);
				slimesSet[i] = false;
			}
		}
	}

	if (gameStates == l2complete)
	{
		resetPattern = true;
		lsplash += frameTime;
		if (lsplash > 1.5)
		{
			gameStates = level3;
			lsplash = 0;
		}
		patternCalled = false;
		Luis.resetHealth();
		aiTime = 0;
	}

	if (gameStates == level3)
	{
		aiTime += frameTime;

		for (int i = 0; i < 3; i++)
		{
			if ((abs(Luis.getCenterX() - skelArr[i].getCenterX())) + (abs(Luis.getCenterX() - skelArr[i].getCenterX())) < 500)
			{
				if (skelArr[i].isAlive())
				{
					setTrack();
				}
			}
			else if (aiTime > 4)
			{
				setPatterns();
				aiTime = 0;
			}
		}

		if (!patternCalled)
		{
			setPatterns();
			patternCalled = true;
		}

		for (int i = 0; i < 3; i++)
		{
			snowTrees[i].setX(-2000);
			snowTrees[i].setY(0);
		}

		if (input->getMouseLButton() == true)
		{
			firstClick = false;
		}
		for (int i = 0; i < 3; i++)
		{
			if (!skelsSet[i])
			{
				skelArr[i].setVisible();
				skelArr[i].setX((float)GAME_WIDTH - (i * 300) - 300);
				skelArr[i].setY((float)i * 180);
				skelsSet[i] = true;
			}
		}

		if (killCount > 8)
		{
			gameStates = end;
			myArrow.setX(-1000);
			myArrow.setY(-2000);
			for (int i = 0; i < 3; i++)
			{
				skelArr[i].setX(-1000);
				skelArr[i].setY(0);
				skelsSet[i] = false;
			}
		}
	}

	/*if (gameStates == characterselect)
	{
	if (input->getMouseY() > charBoxes[0].getY() && input->getMouseY() < charBoxes[0].getY() + charBoxes[0].getHeight()*CHARACTER_BOX_IMAGE_SCALE)
	{
	if (input->getMouseX() > charBoxes[0].getX() && input->getMouseX() < charBoxes[0].getX() + charBoxes[0].getWidth()*CHARACTER_BOX_IMAGE_SCALE) 
	{
	drawb1 = true;
	if (input->getMouseLButton())
	{
	box1clicked = true;
	}
	if (box1clicked)
	{
	gameStates = level1;
	firstClick = true;
	box1clicked = false;
	}
	} 
	else 
	{
	drawb1 = false;
	}

	if (input->getMouseX() > charBoxes[1].getX() && input->getMouseX() < charBoxes[1].getX() + charBoxes[1].getWidth()*CHARACTER_BOX_IMAGE_SCALE) 
	{
	drawb2 = true;
	} 
	else 
	{
	drawb2 = false;
	}

	if (input->getMouseX() > charBoxes[2].getX() && input->getMouseX() < charBoxes[2].getX() + charBoxes[2].getWidth()*CHARACTER_BOX_IMAGE_SCALE) 
	{
	drawb3 = true;
	} 
	else 
	{
	drawb3 = false;
	}
	} 
	else 
	{
	drawb1 = false;
	drawb2 = false;
	drawb3 = false;
	}
	}*/

	if (gameStates == lose)
	{
		if (input->getMouseLButton() == true || input->getMouseRButton() == true)
		{
			lvlTime = 0;
			lsplash = 0;
			firstClick = true;
			killCount = 0;
			gameStates = resetstuff;
			Luis.setX(GAME_WIDTH / 32);
			Luis.setY(GAME_HEIGHT / 32);
			for (int i = 0; i < 3; i++)
			{
				orcArr[i].setVisible();
				skelArr[i].setVisible();
				slimeArr[i].setVisible();
				orcArr[i].setX(-1000);
				orcArr[i].setY(0);
				skelArr[i].setX(-1000);
				skelArr[i].setY(0);
				slimeArr[i].setX(-1000);
				slimeArr[i].setY(0);
				skelsSet[i] = false;
				orcsSet[i] = false;
				slimesSet[i] = false;
			}
		}
	}

	if (gameStates == end)
	{
		resetPattern = true;
		if (input->getMouseLButton() == true || input->getMouseRButton() == true)
		{
			lvlTime = 0;
			firstClick = true;
			killCount = 0;
			gameStates = resetstuff;
			Luis.setX(GAME_WIDTH / 32);
			Luis.setY(GAME_HEIGHT / 32);
			for (int i = 0; i < 3; i++)
			{
				slimeArr[i].setVisible();
				orcArr[i].setVisible();
				skelArr[i].setVisible();
				orcArr[i].setX(-1000);
				orcArr[i].setY(0);
				skelArr[i].setX(-1000);
				skelArr[i].setY(0);
				slimeArr[i].setX(-1000);
				slimeArr[i].setY(0);
				skelsSet[i] = false;
				orcsSet[i] = false;
			}
		}
	}

	if (gameStates == resetstuff)
	{
		up1 = false;
		setLTimer = false;
		luisTimer = 0;
		patternCalled = false;
		l1Timer = 0;
		l2Timer = 0;
		l3Timer = 0;
		gameStates = menu;
		resetPattern = true;
		Luis.resetHealth();
		aiTime = 0;
		for (int i = 0; i < 3; i++)
		{
			orcArr[i].resetHealth();
			slimeArr[i].resetHealth();
			skelArr[i].resetHealth();
			smallTrees[i].setX(-2000);
			smallTrees[i].setY(0);
			snowTrees[i].setX(-2000);
			snowTrees[i].setY(0);
		}
	}
}

//=============================================================================
// Start a new round of play
//=============================================================================
void TreasureQuest::roundStart()
{
	countDownTimer = TreasureQuestNS::COUNT_DOWN;
	countDownOn = true;
	roundOver = false;
}

void createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles)
{
	pm.setPosition(pos);
	pm.setVelocity(vel);
	pm.setVisibleNParticles(numParticles);
}

//=============================================================================
// Update all game items
//=============================================================================
void TreasureQuest::update()
{
	if (resetPattern)
	{
		setPatterns();
		resetPattern = false;
	}

	//gameStateUpdate();

	pm.update(frameTime);

	switch (gameStates)
	{
	case menu:
		if (playMusic)
		{
			audio->playCue(MENU_SONG);
			playMusic = false;
		}

		mainMenu->update();

		if (mainMenu->getSelectedItem() == 0)
		{
			gameStates = level1;
			audio->stopCue(MENU_SONG);
		}
		if (mainMenu->getSelectedItem() == 1)
		{
			gameStates = tutorial;
		}
		break;
	case level1:
		if (!up1)
		{
			Luis.update(frameTime);
			up1 = true;
		}
		if (playOtherMusic)
		{
			audio->playCue(GAME_SONG);
			playOtherMusic = false;
		}
		l1Timer += frameTime;
		if (!firstClick)
		{
			Luis.update(frameTime);
		}
		myArrow.update(frameTime);
		for (int i = 0; i < 3; i++)
		{
			orcArr[i].update(frameTime, Luis, orcCollide[i]);
		}

		if (!myArrow.getVisible())
		{
			myArrow.setX(Luis.getCenterX() - arrowNS::WIDTH / 2);
			myArrow.setY((Luis.getY() + (GENERAL_HEIGHT * GENERAL_IMAGE_SCALE) / 3 + 10));
		}

		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map1[i].update(frameTime);
		}
		{
			map1Sel.update(frameTime);
			float x = (float)input->getMouseX();
			float y = (float)input->getMouseY();
			convertScreenToGrid(x, y);
			map1Sel.setX(x - map1Sel.getWidth() * map1Sel.getScale() / 2);
			map1Sel.setY(y - map1Sel.getHeight() * map1Sel.getScale() / 2);
			map1Sel.setVisible(true);
		}
		break;
	case l1complete:
		//totalTime += l1Timer;
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map1[i].update(frameTime);
		}
		myArrow.update(frameTime);
		up1 = false;
		break;
	case level2:
		l2Timer += frameTime;
		if (!up1)
		{
			Luis.update(frameTime);
			up1 = true;
		}
		if (!firstClick)
		{
			Luis.update(frameTime);
		}
		myArrow.update(frameTime);
		for (int i = 0; i < 3; i++)
		{
			slimeArr[i].update(frameTime, Luis, slimeCollide[i]);
		}

		if (!myArrow.getVisible())
		{
			myArrow.setX(Luis.getCenterX() - arrowNS::WIDTH / 2);
			myArrow.setY((Luis.getY() + (GENERAL_HEIGHT * GENERAL_IMAGE_SCALE) / 3 + 10));
		}

		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map2[i].update(frameTime);
		}
		{
			map2Sel.update(frameTime);
			float x = (float)input->getMouseX();
			float y = (float)input->getMouseY();
			convertScreenToGrid(x, y);
			map2Sel.setX(x - map2Sel.getWidth() * map2Sel.getScale() / 2);
			map2Sel.setY(y - map2Sel.getHeight() * map2Sel.getScale() / 2);
			map2Sel.setVisible(true);
		}
		break;
	case l2complete:
		//totalTime += l2Timer;
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map2[i].update(frameTime);
		}
		myArrow.update(frameTime);
		up1 = false;
		break;
	case level3:
		l3Timer += frameTime;
		if (!up1)
		{
			Luis.update(frameTime);
			up1 = true;
		}
		if (!firstClick)
		{
			Luis.update(frameTime);
		}
		myArrow.update(frameTime);
		for (int i = 0; i < 3; i++)
		{
			skelArr[i].update(frameTime, Luis, skelCollide[i]);
		}

		if (!myArrow.getVisible())
		{
			myArrow.setX(Luis.getCenterX() - arrowNS::WIDTH / 2);
			myArrow.setY((Luis.getY() + (GENERAL_HEIGHT * GENERAL_IMAGE_SCALE) / 3 + 10));
		}

		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map3[i].update(frameTime);
		}
		{
			map3Sel.update(frameTime);
			float x = (float)input->getMouseX();
			float y = (float)input->getMouseY();
			convertScreenToGrid(x, y);
			map3Sel.setX(x - map3Sel.getWidth() * map3Sel.getScale() / 2);
			map3Sel.setY(y - map3Sel.getHeight() * map3Sel.getScale() / 2);
			map3Sel.setVisible(true);
		}
		break;
	case tutorial:
		playMusic = true;
		break;
	case lose:
		myArrow.setX((float)GAME_WIDTH);
		myArrow.setY((float)GAME_HEIGHT);
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map2[i].update(frameTime);
		}
		break;
	case end:
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map3[i].update(frameTime);
		}
		break;
	default:
		if (roundOver)
		{
			roundTimer -= frameTime;
			if (roundTimer <= 0)
				roundStart();
		}
		break;
	}
	gameStateUpdate();
	//Game::update();
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void TreasureQuest::ai()
{
	for (int i = 0; i < 3; i++)
	{
		skelArr[i].ai(frameTime, Luis);
		orcArr[i].ai(frameTime, Luis);
		slimeArr[i].ai(frameTime, Luis);
	}

	for (int i = 0; i < 9; i++)
	{
		if (patternStepIndex == maxPatternSteps)
			patternStepIndex = 0;
		if (steps[i][patternStepIndex].isFinished())
			patternStepIndex++;
		steps[i][patternStepIndex].update(frameTime);
	}
}

//=============================================================================
// Handle collisions
//=============================================================================
void TreasureQuest::collisions()
{
	VECTOR2 collisionVector;
	for (int i = 0; i < 3; i++)
	{
		if (skelArr[i].collidesWith(Luis, collisionVector) || orcArr[i].collidesWith(Luis, collisionVector) || slimeArr[i].collidesWith(Luis, collisionVector))
		{
			setLTimer = true;
			//Luis.loseHealth();
			if (!Luis.isAlive())
			{
				dead = true;
				gameStates = lose;
			}
			//lives--;
		}

		if (setLTimer)
		{
			luisTimer += frameTime;
			if (luisTimer > 1)
			{
				setLTimer = false;
				luisTimer = 0;
				Luis.loseHealth();
				VECTOR2 foo, bar;

				foo = VECTOR2(Luis.getCenterX(), Luis.getCenterY());
				bar = VECTOR2(80, 50);
				createParticleEffect(foo, bar, 200);
			}
		}

		VECTOR2 collisionVector;
		VECTOR2 foo, bar;

		if (slimeArr[i].collidesWith(myArrow, collisionVector))
		{
			//audio->playCue(CRY_SONG);
			if (Luis.getY() < slimeArr[i].getY())
			{
				foo = VECTOR2(myArrow.getX() + myArrow.getWidth() - 20, myArrow.getCenterY() + 20);
			}
			else
			{
				foo = VECTOR2(myArrow.getX() + myArrow.getWidth() - 20, myArrow.getCenterY() - 20);
			}
			//foo = VECTOR2(skelArr[i].getX() + skelArr[i].getWidth() - 20, skelArr[i].getCenterY());
			bar = VECTOR2(80, 50);
			createParticleEffect(foo, bar, 200);
			if (!contactMade)
			{
				audio->playCue(CRY_SONG);
				//healthTimer += frameTime;
				slimeArr[i].loseHealth();
				slimeArr[i].loseHealth();
				slimeArr[i].loseHealth();
				contactMade = true;
				setSkelTime = true;
			}
			if (!slimeArr[i].isAlive())
			{
				slimeArr[i].setInvisible();
				killCount++;
			}
		}

		if (skelArr[i].collidesWith(myArrow, collisionVector))
		{
			//audio->playCue(CRY_SONG);
			if (Luis.getY() < skelArr[i].getY())
			{
				foo = VECTOR2(myArrow.getX() + myArrow.getWidth() - 20, myArrow.getCenterY() + 20);
			}
			else
			{
				foo = VECTOR2(myArrow.getX() + myArrow.getWidth() - 20, myArrow.getCenterY() - 20);
			}
			//foo = VECTOR2(skelArr[i].getX() + skelArr[i].getWidth() - 20, skelArr[i].getCenterY());
			bar = VECTOR2(80, 50);
			createParticleEffect(foo, bar, 200);
			if (!contactMade)
			{
				audio->playCue(CRY_SONG);
				//healthTimer += frameTime;
				skelArr[i].loseHealth();
				skelArr[i].loseHealth();
				contactMade = true;
				setSkelTime = true;
			}
			if (!skelArr[i].isAlive())
			{
				skelArr[i].setInvisible();
				killCount++;
			}
		}

		if (setSkelTime)
		{
			healthTimer += frameTime;
			if (healthTimer > 1.0)
			{
				contactMade = false;
				setSkelTime = false;
				healthTimer = 0;
			}
		}

		if (orcArr[i].collidesWith(myArrow, collisionVector))
		{
			//audio->playCue(CRY_SONG);
			if (Luis.getY() < orcArr[i].getY())
			{
				foo = VECTOR2(myArrow.getX() + myArrow.getWidth() - 20, myArrow.getCenterY() + 20);
			}
			else
			{
				foo = VECTOR2(myArrow.getX() + myArrow.getWidth() - 20, myArrow.getCenterY() - 20);
			}
			//foo = VECTOR2(skelArr[i].getX() + skelArr[i].getWidth() - 20, skelArr[i].getCenterY());
			bar = VECTOR2(80, 50);
			createParticleEffect(foo, bar, 200);
			if (!contactMade)
			{
				audio->playCue(CRY_SONG);
				//healthTimer += frameTime;
				orcArr[i].loseHealth();
				orcArr[i].loseHealth();
				orcArr[i].loseHealth();
				orcArr[i].loseHealth();
				orcArr[i].loseHealth();
				orcArr[i].loseHealth();
				contactMade = true;
				setSkelTime = true;
			}
			if (!orcArr[i].isAlive())
			{
				orcArr[i].setInvisible();
				killCount++;
			}
		}

		if (myArrow.collidesWith(smallTrees[i], collisionVector) || myArrow.collidesWith(snowTrees[i], collisionVector))
		{
			myArrow.setX(700);
			myArrow.setY(-700);
		}
	}
}

//=============================================================================
// Render game items
//=============================================================================
void TreasureQuest::render()
{
	graphics->spriteBegin(); // begin drawing sprites

	totalTime = l1Timer + l2Timer + l3Timer;
	std::string s = std::to_string(killCount);
	std::string t = std::to_string((int)l1Timer);
	std::string r = std::to_string((int)l2Timer);
	std::string u = std::to_string((int)totalTime);

	switch (gameStates)
	{
	case menu:
		menuImage.draw();
		mainMenu->displayMenu();
		break;
	case tutorial:
		menuImage.draw();
		tutorialText->setFontColor(graphicsNS::WHITE);
		tutorialText->print("Move your character by left-clicking on a tile", 60, 50);
		tutorialText->print("Aim by right-clicking near an enemy", 60, 110);
		break;

	case level1:
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map1[i].draw();
		}
		map1Sel.draw();
		myArrow.draw();
		Luis.draw();

		for (int i = 0; i < 3; i++)
		{
			if ((Luis.getY() + Luis.getHeight() * GENERAL_IMAGE_SCALE) < (smallTrees[i].getY() + smallTrees[i].getHeight() * SMALL_TREES_SCALE + 50))
			{
				//Luis.draw();
				smallTrees[i].draw();
			}
			else
			{
				smallTrees[i].draw();
				Luis.draw();
			}
			orcArr[i].draw();
		}
		pm.draw();
		break;
	case l1complete:
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map1[i].draw();
		}
		tutorialText->print("Level 1 Complete!", 400, 250);
		tutorialText->print("Total time: ", 400, 310);
		tutorialText->print(t, 680, 310);
		break;

	case level2:

		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map2[i].draw();
		}
		map2Sel.draw();
		myArrow.draw();
		Luis.draw();

		for (int i = 0; i < 3; i++)
		{
			if ((Luis.getY() + Luis.getHeight() * GENERAL_IMAGE_SCALE) < (snowTrees[i].getY() + snowTrees[i].getHeight() * SNOW_TREES_SCALE + 100))
			{
				//Luis.draw();
				snowTrees[i].draw();
			}
			else
			{
				snowTrees[i].draw();
				Luis.draw();
			}
			slimeArr[i].draw();
		}
		pm.draw();
		break;

	case l2complete:
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map2[i].draw();
		}
		tutorialText->print("Level 2 Complete!", 400, 250);
		tutorialText->print("Total time: ", 400, 310);
		tutorialText->print(r, 680, 310);
		break;

	case level3:

		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map3[i].draw();
		}
		map3Sel.draw();
		myArrow.draw();
		Luis.draw();

		for (int i = 0; i < 3; i++)
		{
			if ((Luis.getY() + Luis.getHeight() * GENERAL_IMAGE_SCALE) < (smallTrees[i].getY() + smallTrees[i].getHeight() * SMALL_TREES_SCALE + 20))
			{
				//Luis.draw();
				smallTrees[i].draw();
			}
			else
			{
				smallTrees[i].draw();
				Luis.draw();
			}
			skelArr[i].draw();
		}
		pm.draw();
		break;
	case lose:
		l1Timer = 0;
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map3[i].draw();
		}
		tutorialText->print("You died!", 100, 50);
		tutorialText->print("Enemies killed:", 100, 110);
		tutorialText->print(s, 500, 110);
		tutorialText->print("Click anywhere to play again", 100, 170);
		tutorialText->print("Reminder: Move character by left-clicking", 100, 250);
		tutorialText->print("Aim by right-clicking near an enemy", 100, 310);
		break;
	case end:
		for (int i = 0; i < MAP_IMAGE_ROWS * MAP_IMAGE_COLUMNS; i++)
		{
			map3[i].draw();
		}
		tutorialText->print("Congratulations, you made it!", 100, 50);
		tutorialText->print("Enemies killed:", 100, 110);
		tutorialText->print(s, 500, 110);
		tutorialText->print("Total time:", 100, 170);
		tutorialText->print(u, 385, 170);
		tutorialText->print("Click anywhere to play again", 100, 230);
		tutorialText->print("Reminder: Move character by left-clicking", 100, 290);
		tutorialText->print("Aim by right-clicking near an enemy", 100, 350);
		break;
	default:
		break;
	}

	if (countDownOn)
	{
		_snprintf_s(buffer, TreasureQuestNS::BUF_SIZE, "%d", (int)(ceil(countDownTimer)));
		fontBig.print(buffer, TreasureQuestNS::COUNT_DOWN_X, TreasureQuestNS::COUNT_DOWN_Y);
	}

	graphics->spriteEnd(); // end drawing sprites
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void TreasureQuest::releaseAll()
{
	fontBig.onLostDevice();
	menuTexture.onLostDevice();
	archerTexture.onLostDevice();
	arrowTexture.onLostDevice();
	skelTexture.onLostDevice();
	slimeTexture.onLostDevice();
	map1Texture.onLostDevice();
	map1SelTexture.onLostDevice();
	map2Texture.onLostDevice();
	map2SelTexture.onLostDevice();
	map3Texture.onLostDevice();
	map3SelTexture.onLostDevice();
	orcTexture.onLostDevice();
	logoTexture.onLostDevice();
	smallTreesTexture.onLostDevice();
	snowTreesTexture.onLostDevice();
	tutorialText->onLostDevice();
	healthTexture.onLostDevice();

	Game::releaseAll();
	return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void TreasureQuest::resetAll()
{
	fontBig.onResetDevice();
	menuTexture.onResetDevice();
	archerTexture.onResetDevice();
	arrowTexture.onResetDevice();
	skelTexture.onResetDevice();
	map1Texture.onResetDevice();
	map1SelTexture.onResetDevice();
	map2Texture.onResetDevice();
	map2SelTexture.onResetDevice();
	map3Texture.onResetDevice();
	map3SelTexture.onResetDevice();
	orcTexture.onResetDevice();
	slimeTexture.onResetDevice();
	logoTexture.onResetDevice();
	smallTreesTexture.onResetDevice();
	snowTreesTexture.onResetDevice();
	tutorialText->onResetDevice();
	healthTexture.onResetDevice();

	Game::resetAll();
	return;
}

//=============================================================================
// Delete all reserved memory
//=============================================================================
void TreasureQuest::deleteAll()
{
	SAFE_DELETE(dxFontSmall);
	SAFE_DELETE(mainMenu);
	SAFE_DELETE(subMenu);
	SAFE_DELETE(tutorialText);
	Game::deleteAll();
}

void TreasureQuest::setPatterns()
{
	if (gameStates == level1)
	{
		for (int j = 0; j < 9; j++)
		{
			patternStepIndex = 0;
			for (int i = 0; i < maxPatternSteps; i++)
			{
				steps[j][i].setActive();
			}
			steps[j][0].setAction(DOWN);
			steps[j][0].setTimeForStep(1);
			steps[j][1].setAction(LEFT);
			steps[j][1].setTimeForStep(1);
			steps[j][2].setAction(UP);
			steps[j][2].setTimeForStep(1);
			steps[j][3].setAction(RIGHT);
			steps[j][3].setTimeForStep(1);
		}
	}

	if (gameStates == level2)
	{
		for (int j = 0; j < 9; j++)
		{
			patternStepIndex = 0;
			for (int i = 0; i < maxPatternSteps; i++)
			{
				steps[j][i].setActive();
			}
			steps[j][0].setAction(UP);
			steps[j][0].setTimeForStep(1);
			steps[j][1].setAction(UP);
			steps[j][1].setTimeForStep(1);
			steps[j][2].setAction(DOWN);
			steps[j][2].setTimeForStep(1);
			steps[j][3].setAction(DOWN);
			steps[j][3].setTimeForStep(1);
		}
	}

	if (gameStates == level3)
	{
		for (int j = 0; j < 9; j++)
		{
			patternStepIndex = 0;
			for (int i = 0; i < maxPatternSteps; i++)
			{
				steps[j][i].setActive();
			}
			steps[j][0].setAction(DIAGINV);
			steps[j][0].setTimeForStep(1);
			steps[j][1].setAction(DIAGINV);
			steps[j][1].setTimeForStep(1);
			steps[j][2].setAction(DIAGONAL);
			steps[j][2].setTimeForStep(1);
			steps[j][3].setAction(DIAGONAL);
			steps[j][3].setTimeForStep(1);
		}
	}
}

void TreasureQuest::setTrack()
{
	if (gameStates == level1)
	{
		for (int j = 0; j < 9; j++)
		{
			patternStepIndex = 0;
			for (int i = 0; i < maxPatternSteps; i++)
			{
				steps[j][i].setActive();
			}
			steps[j][0].setAction(TRACK);
			steps[j][0].setTimeForStep(30);
		}
	}

	if (gameStates == level2)
	{
		for (int j = 0; j < 9; j++)
		{
			patternStepIndex = 0;
			for (int i = 0; i < maxPatternSteps; i++)
			{
				steps[j][i].setActive();
			}
			steps[j][0].setAction(TRACK);
			steps[j][0].setTimeForStep(30);
		}
	}

	if (gameStates == level3)
	{
		for (int j = 0; j < 9; j++)
		{
			patternStepIndex = 0;
			for (int i = 0; i < maxPatternSteps; i++)
			{
				steps[j][i].setActive();
			}
			steps[j][0].setAction(TRACK);
			steps[j][0].setTimeForStep(30);
		}
	}
}