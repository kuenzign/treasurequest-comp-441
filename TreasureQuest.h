#ifndef _TREASUREQUEST_H // Prevent multiple definitions if this
#define _TREASUREQUEST_H // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <string>
#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "menu_Kuenzig.h"
#include "archer_Venegas.h"
#include "Skeleton_Messinger.h"
#include "pointer.h"
#include "orc_Messinger.h"
#include <vector>
#include "Logo.h"
#include "GeneralEntity_Kuenzig.h"
#include "Slime_Kuenzig.h"
#include "patternStep.h"

#define maxPatternSteps 5

namespace TreasureQuestNS
{
	const RECT SMALL_RECTANGLE = {-30, -50, 30, 50};
	const RECT SNOW_RECTANGLE = {-30, -60, 30, 70};
	const char FONT[] = "Arial Bold"; // font
	const int FONT_BIG_SIZE = 256;    // font height
	const COLOR_ARGB FONT_COLOR = graphicsNS::YELLOW;
	const int COUNT_DOWN_X = GAME_WIDTH / 2 - FONT_BIG_SIZE / 4;
	const int COUNT_DOWN_Y = GAME_HEIGHT / 2 - FONT_BIG_SIZE / 2;
	const int COUNT_DOWN = 2; // count down
	const int BUF_SIZE = 20;
	const int ROUND_TIME = 5; // time until new round starts
}

// TreasureQuest is the class we create, it inherits from the Game class
class TreasureQuest : public Game
{
private:
	// game items
	TextureManager menuTexture; // textures
	Image menuImage;		// menu image
	TextureManager subMenuTexture;
	Image subMenuImage;
	TextDX *tutorialText;

	//keep track of one iteration
	bool playMusic;
	bool playOtherMusic;

	bool orcCollide[100];
	bool skelCollide[100];
	bool slimeCollide[100];

	bool background_and_ready_game;

	bool countDownOn; // true when count down is displayed
	float countDownTimer;
	TextDX fontBig; // DirectX font for game banners
	char buffer[TreasureQuestNS::BUF_SIZE];
	bool roundOver;   // true when round is over
	float roundTimer; // time until new round starts
	float stateTimer;
	int killCount;
	bool firstClick;
	float lvlTime;
	bool skelsSet[3];
	bool orcsSet[3];
	bool slimesSet[3];
	float lsplash;
	float l1Timer;
	float l2Timer;
	float l3Timer;
	float totalTime;
	bool contactMade;
	float healthTimer;
	bool setSkelTime;
	float positions1[6];
	float positions2[6];
	float treeVals[20];
	bool patternCalled;
	float luisTimer;
	bool setLTimer;
	bool up1;
	float aiTime;
	int selection;
	bool orcAI[9];

	bool playGameMusic;

	GameStates gameStates;
	float timeInState;
	void gameStateUpdate();

	int score;
	TextDX *dxFontSmall;
	bool dead;

	//Menu stuff
	Menu *mainMenu;
	Menu *subMenu;
	TextDX *output;
	std::string outString;

	//Level1 stuff
	Archer Luis;
	TextureManager archerTexture;
	TextureManager archerIdle;
	TextureManager map1Texture;
	TextureManager map2Texture;
	TextureManager map3Texture;
	Image map1[145];
	Image map2[145];
	Image map3[145];
	TextureManager map1SelTexture;
	TextureManager map2SelTexture;
	TextureManager map3SelTexture;
	Image map1Sel;
	Image map2Sel;
	Image map3Sel;
	TextureManager arrowTexture;
	arrow myArrow; //bad idea, bad programming
	Logo logo;
	TextureManager logoTexture;
	TextureManager smallTreesTexture;
	Entity smallTrees[10];
	TextureManager snowTreesTexture;
	Entity snowTrees[10];

	//enemy stuff
	TextureManager slimeTexture;
	Slime slimeArr[3];
	TextureManager skelTexture;
	Skeleton skelArr[3];
	Orc orcArr[3];
	TextureManager orcTexture;
	//aiManager_Venegas* aiManagerArr[6];
	PatternStep steps[9][maxPatternSteps];
	int patternStepIndex;

	bool resetPattern;

	TextureManager healthTexture;

public:
	// Constructor
	TreasureQuest();
	// Destructor
	virtual ~TreasureQuest();
	// Initialize the game
	void initialize(HWND hwnd);
	void update();     // must override pure virtual from Game
	void ai();	 // "
	void collisions(); // "
	void render();     // "
	void roundStart(); // start a new round of play
	void releaseAll();
	void resetAll();
	void deleteAll();
	void setPatterns();
	void setTrack();
};

#endif