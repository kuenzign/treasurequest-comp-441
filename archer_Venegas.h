#ifndef _ARCHCHERVENEGAS_H
#define _ARCHCHERVENEGAS_H
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "input.h"
#include "graphics.h"
#include "game.h"
#include "hero_Kuenzig.h"
#include "arrow.h"

namespace archerNS
{
	//const RECT  COLLISION_RECTANGLE = {-64, -64, 64, 64};
	const float SPEED = 100;                // 200 pixels per second
}

class Archer : public Hero
{
public:
	Archer(void);

	virtual void attack(void);

	// inherited member functions
	virtual void update(float frameTime);

	VECTOR2 getVelocity() {return velocity;}

private:
	//enum LastDirection {left, right} lastDirection;
	//enum CurrentDirection {up, down, none} currentDirection;
	//VECTOR2 velocity;
	float speed;
	float targetX;
	float targetY;

	bool startShoot;

	arrow myArrow;
};

#endif