#ifndef _SLIME_H
#define _SLIME_H
#define WIN32_LEAN_AND_MEAN

#include "enemy_messinger.h"
#include "hero_Kuenzig.h"

class Slime : public Enemy
{
public:
	Slime(void);

	virtual void doInvincible(void);

	// Update Slime.
	// typically called once per frame
	// frameTime is used to regulate the speed of movement and animation
	virtual void update(float frameTime, Hero target, bool &collide);

private:
	float timer;
	float switchTime;
};

#endif