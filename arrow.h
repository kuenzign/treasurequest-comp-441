#pragma once
#include "weapon.h"
#include <ctime>

namespace arrowNS{
	const int WIDTH = 76;                   // image width in pixels
	const int HEIGHT = 10;                  // image height in pixels
	const float SPEED = 800;                // 800 pixels per second
	const float ARROW_IMAGE_SCALE = 1.0f;
}

class arrow :
	public weapon
{
public:
	arrow(void);
	~arrow(void);
	void update(float frameTime);
	bool arrow::initialize(Game *gamePtr, int width, int height, int ncols, TextureManager *textureM);
	void moveOffScreen();
private:
	float clickX;
	float clickY;
	bool moveArrow;
	float squareCalc;
	D3DXVECTOR2 shot;
	float timeCount;
};

