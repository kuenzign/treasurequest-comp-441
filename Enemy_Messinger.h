#ifndef _ENEMY_H
#define _ENEMY_H
#define WIN32_LEAN_AND_MEAN

#include "GeneralEntity_Kuenzig.h"
#include "hero_Kuenzig.h"

namespace enemyNS
{
	const float SPEED_X = 0;
	const float SPEED_Y = -0;
}

class Enemy : public GeneralEntity
{
private:
	bool collision;
	bool target;
	float speed;
	GeneralEntity targetEntity;

public:
	// constructor
	Enemy();

	// inherited member functions
	virtual void update(float frameTime, Hero target, bool &collide);

	void trackSpot(); //float x, float y, float frameTime, bool &collide);

	// Set collision Boolean
	void setCollision(bool c) { collision = c; }

	// Set collision type (NONE, CIRCLE, BOX, ROTATED_BOX)
	virtual void setCollisionType(entityNS::COLLISION_TYPE ctype) { collisionType = ctype; }

	// Set RECT structure used for BOX and ROTATED_BOX collision detection.
	void setEdge(RECT e) { edge = e; }

	// Set target
	void setTarget(bool t) { target = t; }

	// Get collision
	bool getCollision() { return collision; }

	// Get collision type
	entityNS::COLLISION_TYPE getCollisionType() { return collisionType; }

	// Does this entity collide with ent?
	virtual bool collidesWith(Hero &ent, VECTOR2 &collisionVector);
	virtual bool collidesWith(GeneralEntity &ent, VECTOR2 &collisionVector);
    virtual bool collidesWith(Entity &ent, VECTOR2 &collisionVector);

	void setInvisible();

	void setVisible();

	void ai(float time, GeneralEntity &t);

	void vectorTrack();
	void deltaTrack();
	void evade();
};

#endif