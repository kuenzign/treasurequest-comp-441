#include "menu_Kuenzig.h"

//=============================================================================
// Constructor
//=============================================================================
Menu::Menu()
{
	selectedItem = -1; //nothing return
	menuItemFont = new TextDX();
	menuHeadingFont = new TextDX();
}

//=============================================================================
// Initializes the menu
//=============================================================================
void Menu::initialize(Graphics *g, Input *i, std::string heading, std::vector<menuItem> items)
{
	menuHeading = heading;
	menuItems = items;
	highlightColor = graphicsNS::GREEN;
	normalColor = graphicsNS::YELLOW;
	menuAnchor = D3DXVECTOR2(65, 60);
	input = i;
	verticalOffset = 140;
	horizontalOffset = 1000;
	linePtr = 0;
	selectedItem = -1;
	graphics = g;
	menuItemFont = new TextDX();
	menuHeadingFont = new TextDX();
	menuItemFontHighlight = new TextDX();
	if (menuItemFont->initialize(graphics, 70, true, false, "Bit Darling10 (sRB)") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if (menuItemFontHighlight->initialize(graphics, 70, true, false, "Bit Darling10 (sRB)") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if (menuHeadingFont->initialize(graphics, 70, true, false, "Bit Darling10 (sRB)") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuHeading font"));
	menuHeadingFont->setFontColor(normalColor);
	menuItemFont->setFontColor(normalColor);
	menuItemFontHighlight->setFontColor(highlightColor);

	if (!menuBoxTexture.initialize(graphics, MENU_BOX))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Menu box texture initialization failed"));
	menuBoxImages.resize(menuItems.size());
	for (auto iter = menuBoxImages.begin(); iter != menuBoxImages.end(); iter++)
	{
		if (!(*iter).initialize(graphics, 0, 0, 0, &menuBoxTexture))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error init menu box"));
		(*iter).setX(menuAnchor.x + 65);
		(*iter).setY(menuAnchor.y + (iter - menuBoxImages.begin() + 1) * verticalOffset - 25);
		(*iter).setScale(MENU_BOX_SCALE);
	}
}

//=============================================================================
// Update all menu items
//=============================================================================
void Menu::update()
{
	if (selectedItem >= 0 && selectedItem < (int)menuItems.size() && menuItems[selectedItem].subMenu != nullptr)
	{
		menuItems[selectedItem].subMenu->update();
	}
	else
	{
		selectedItem = -1;
		for (int i = 0; i < (int)menuItems.size(); i++)
		{
			if (input->getMouseY() > menuAnchor.y + (i + 1) * verticalOffset && input->getMouseY() < menuAnchor.y + (i + 2) * verticalOffset && input->getMouseX() > menuAnchor.x && input->getMouseX() < menuAnchor.x + horizontalOffset)
			{
				linePtr = i;
				if (input->getMouseLButton())
				{
					selectedItem = linePtr;
					input->setMouseLButton(false);
				}
			}
		}
		if (input->wasKeyPressed(VK_UP))
		{
			linePtr--;
		}
		if (input->wasKeyPressed(VK_DOWN))
		{
			linePtr++;
		}

		if (linePtr < 0)
		{
			linePtr = menuItems.size() - 1;
		}
		if (linePtr >= (int)menuItems.size())
		{
			linePtr = 0;
		}

		if (input->isKeyDown(VK_RETURN))
		{
			selectedItem = linePtr;
		}
	}
}

int Menu::getSelectedItem()
{
	if (selectedItem >= 0 && selectedItem < (int)menuItems.size() && menuItems[selectedItem].subMenu != nullptr)
	{
		return menuItems[selectedItem].subMenu->getSelectedItem();
	}
	else
	{
		return selectedItem;
	}
}

//=============================================================================
// Render menu items
//=============================================================================
void Menu::displayMenu()
{
	if (selectedItem >= 0 && selectedItem < (int)menuItems.size() && menuItems[selectedItem].subMenu != nullptr)
	{
		menuItems[selectedItem].subMenu->displayMenu();
	}
	else
	{
		menuHeadingFont->print(menuHeading, (int)menuAnchor.x, (int)menuAnchor.y);
		for (int i = 0; i < (int)menuItems.size(); i++)
		{
			menuBoxImages[i].draw();
			if (linePtr == i)
			{
				menuItemFontHighlight->print(menuItems[i].name, (int)menuAnchor.x + 90, +(int)menuAnchor.y + (i + 1) * verticalOffset);
			}
			else
			{
				menuItemFont->print(menuItems[i].name, (int)menuAnchor.x + 90, (int)menuAnchor.y + (i + 1) * verticalOffset);
			}
		}
	}
}

//=============================================================================
// Destructor
//=============================================================================
Menu::~Menu()
{
	SAFE_DELETE(menuItemFont);
	SAFE_DELETE(menuHeadingFont);
	SAFE_DELETE(menuItemFontHighlight);
}