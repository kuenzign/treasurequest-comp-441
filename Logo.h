// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 planet.h v1.0

#ifndef _LOGO_H              // Prevent multiple definitions if this 
#define _LOGO_H              // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "entity.h"
#include "constants.h"

namespace logoNS
{
    const int   WIDTH = 1850;               
    const int   HEIGHT = 268;               
	const float   LOGO_IMAGE_SCALE = 0.5f;
    const int   X = GAME_WIDTH/2 - WIDTH/2; // location on screen
    const int   Y = GAME_HEIGHT/2 - HEIGHT/2;
}

class Logo : public Entity            
{
public:
    Logo();
};
#endif