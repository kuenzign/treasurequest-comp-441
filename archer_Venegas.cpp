#include "archer_Venegas.h"

//=============================================================================
// default constructor
//=============================================================================
Archer::Archer() : Hero()
{
	attacking = false;
	startShoot = false;
}

void Archer::attack(){
	if(startShoot){
		setCurrentFrame(ARCHER_SHOOTING_START*GENERAL_NUM_DIRECTIONS+direc);
		startShoot = false;
	}
	setFrames(ARCHER_SHOOTING_START*GENERAL_NUM_DIRECTIONS+direc, ARCHER_SHOOTING_END*GENERAL_NUM_DIRECTIONS+direc);

	if (getCurrentFrame() == ARCHER_SHOOTING_END*GENERAL_NUM_DIRECTIONS+direc){
		attacking = false;
	}
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Archer::update(float frameTime)
{
	//Implement shooting here, the attacks for all the characters will be different
	if (input->getMouseRButton())
	{
		walking = false;
		targetX = (float)input->getMouseX();
		targetY = (float)input->getMouseY();
		attacking = true;
		startShoot = true;
	}

	Hero::update(frameTime);
}