#ifndef _GENERALENTITY_H                // Prevent multiple definitions if this 
#define _GENERALENTITY_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <string>
#include "entity.h"

namespace generalEntityNS
{
	/*Should probably change this to a more sophisticated collision detector*/
	const RECT  COLLISION_RECTANGLE = {-GENERAL_COLLISION_WIDTH/2, -GENERAL_COLLISION_HEIGHT/2, GENERAL_COLLISION_WIDTH/2, GENERAL_COLLISION_HEIGHT/2};

	/*Where we spawn our entities*/
	const int X = 100;   // location on screen
	const int Y = 100;
	const float SPEED = 150;                // 150 pixels per second
	const int MAX_HEALTH = 6;
}

class GeneralEntity : public Entity
{
public:
	// Constructor
	GeneralEntity();
	// Destructor
	virtual ~GeneralEntity();

	////////////////////////////////////////
	//           Get functions            //
	////////////////////////////////////////

	// Return bottom center X.
	virtual float getPosX(void)			{return spriteData.x + (GENERAL_TILE_WIDTH*spriteData.scale/2);}

	// Return bottom center Y.
	virtual float getPosY(void)			{return spriteData.y + (GENERAL_HEIGHT*spriteData.scale) - (GENERAL_TILE_HEIGHT*spriteData.scale/2);}

	virtual const VECTOR2 getPos(void)  {return VECTOR2(getPosX(), getPosY());}

	virtual int getHealth(void)			{return health;}

	////////////////////////////////////////
	//           Set functions            //
	////////////////////////////////////////

	// Set bottom center X location.
	virtual void setPosX(float newX)   {spriteData.x = newX - (GENERAL_TILE_WIDTH*spriteData.scale/2);}

	// Set bottom center Y location.
	virtual void setPosY(float newY)   {spriteData.y = newY + (GENERAL_TILE_HEIGHT*spriteData.scale/2) - (GENERAL_HEIGHT*spriteData.scale);}

	// Set invincibility.
	virtual void setInvincible(bool i)   {invincible = i;}

	// Set attacking.
	virtual void setAttack(bool a)   {attacking = a;}

	////////////////////////////////////////
	//         Other functions            //
	////////////////////////////////////////

	// Update Entity.
	// typically called once per frame
	// frameTime is used to regulate the speed of movement and animation
	virtual void update(float frameTime);

	// Initialize Entity
	// Pre: *gamePtr = pointer to Game object
	//      *textureM = pointer to TextureManager object
	//      *healthTexture = pointer to health's TextureManager object
	virtual bool initialize(Game *gamePtr, TextureManager *textureM, TextureManager *healthTexture);

	virtual void draw(void);
	virtual void resetHealth(void)	{health = generalEntityNS::MAX_HEALTH;}
	virtual void loseHealth(void)	{ if(!invincible && health > 0) health--; }
	virtual void gainHealth(void)	{ if(health < generalEntityNS::MAX_HEALTH) health++; }
	virtual bool isAlive(void)		{return health > 0;}
	virtual bool isInvincible(void)	{return invincible;}

	virtual void doInvincible(void);
	virtual void walk(void);
	virtual void attack(void);
	virtual void rest(void);

	// Does this entity collide with ent?
	virtual bool collidesWith(GeneralEntity &ent, VECTOR2 &collisionVector);
    virtual bool collidesWith(Entity &ent, VECTOR2 &collisionVector);

protected:
	Image healthImage;
	int health;

	bool walking;
	bool attacking;
	bool invincible;

	direction direc;

	float targetX;
	float targetY;
};

#endif