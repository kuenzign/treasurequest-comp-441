#include "Enemy_Messinger.h"

//=============================================================================
// default constructor
//=============================================================================
Enemy::Enemy() : GeneralEntity()
{
	velocity = D3DXVECTOR2(0, 0);

	collision = false;
	collisionType = entityNS::BOX;
	target = false;
}

void Enemy::setInvisible()
{
	Image::setVisible(false);
	active = false;
}

void Enemy::setVisible()
{
	Image::setVisible(true);
	active = true;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Enemy::update(float frameTime, Hero target, bool &collide)
{
	if(getPosX() < targetEntity.getPosX())
	{
		if(getPosY() < targetEntity.getPosY())
		{
			direc = downright;
		}
		else
		{
			direc = upright;
		}
	}		
	else
	{
		if(getPosY() < targetEntity.getPosY())
		{
			direc = downleft;
		}
		else
		{
			direc = upleft;
		}
	}

	spriteData.x += frameTime * velocity.x * 70;
	spriteData.y += frameTime * velocity.y * 70;

	collide = false;
	GeneralEntity::update(frameTime);
}

void Enemy::evade()
{
	VECTOR2 myVel = D3DXVECTOR2(0, 0);
	VECTOR2 target = targetEntity.getPos();

	if (getPosY() > .5 * target.y)
	{
		myVel.y = -1;
	}
	else
		myVel.y = 0;
	if (getPosX() > .5 * target.x)
	{
		myVel.x = -1;
	}
	else
		myVel.x = 0;
	Graphics::Vector2Normalize(&myVel); //D3DXVec2Normailze(&vel, &vel);
	setVelocity(myVel);
}

void Enemy::deltaTrack()
{
	VECTOR2 myVel = D3DXVECTOR2(1, 1);
	VECTOR2 target = targetEntity.getPos();
	if (getPosY() > target.y)
	{
		myVel.y *= -1;
	}
	else if (getPosY() == target.y)
	{
		myVel.y = 0;
	}
	if (getPosX() > target.x)
	{
		myVel.x *= -1;
	}
	else if (getPosX() == target.x)
	{
		myVel.x = 0;
	}
	Graphics::Vector2Normalize(&myVel); //D3DXVec2Normailze(&vel, &vel);
	setVelocity(myVel);
}
void Enemy::vectorTrack()
{
	VECTOR2 dir = targetEntity.getPos() - getPos(); //Not what we'll do anymroe
	Graphics::Vector2Normalize(&dir);
	setVelocity(dir);
}

void Enemy::ai(float time, GeneralEntity &t)
{
	targetEntity = t;
	//deltaTrack();
	//vectorTrack();
	//evade();
	return;
}

void Enemy::trackSpot()
{ //float x, float y, float frameTime, bool &collide){

	float x = targetEntity.getPosX();
	float y = targetEntity.getPosY();

	VECTOR2 vel = VECTOR2(getPosX(), getPosY()) - VECTOR2(x, y);
	VECTOR2 *foo = D3DXVec2Normalize(&vel, &vel);
	setVelocity(-vel);
}

//=============================================================================
// Perform collision detection between this entity and the other Entity.
// Each entity must use a single collision type. Complex shapes that require
// multiple collision types may be done by treating each part as a separate
// entity.
// Typically called once per frame.
// The collision types: CIRCLE, BOX, or ROTATED_BOX.
// Post: returns true if collision, false otherwise
//       sets collisionVector if collision
//=============================================================================
bool Enemy::collidesWith(Hero &ent, VECTOR2 &collisionVector){
	bool collide = GeneralEntity::collidesWith(ent, collisionVector);
	setAttack(collide);
	return collide;
}
bool Enemy::collidesWith(GeneralEntity &ent, VECTOR2 &collisionVector){
	return GeneralEntity::collidesWith(ent, collisionVector);
}
bool Enemy::collidesWith(Entity &ent, VECTOR2 &collisionVector){
	return GeneralEntity::collidesWith(ent, collisionVector);
}