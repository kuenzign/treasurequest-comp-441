#include "GeneralEntity_Kuenzig.h"


//=============================================================================
// default constructor
//=============================================================================
GeneralEntity::GeneralEntity() : Entity(){
	spriteData.width = GENERAL_WIDTH;           // size of Entity
	spriteData.height = GENERAL_HEIGHT;
	spriteData.scale = GENERAL_IMAGE_SCALE;

	float x = generalEntityNS::X;
	float y = generalEntityNS::Y;
	convertScreenToGrid(x, y);     // translate to grid
	setPosX(x);
	setPosY(y);

	frameDelay = GENERAL_ANIMATION_DELAY;

	spriteData.rect.top = 0;    // rectangle to select parts of an image
	spriteData.rect.left = 0;
	spriteData.rect.bottom = GENERAL_HEIGHT;
	spriteData.rect.right = GENERAL_WIDTH;

	velocity.x = 0;                             // velocity X
	velocity.y = 0;                             // velocity Y

	collisionType = entityNS::BOX;
	edge = generalEntityNS::COLLISION_RECTANGLE;

	targetX = getX();
	targetY = getY();

	walking = false;
	attacking = false;

	health = generalEntityNS::MAX_HEALTH;
	invincible = false;
}

GeneralEntity::~GeneralEntity(void)
{
}

//=============================================================================
// Initialize the Entity.
// Pre: *gamePtr = pointer to Game object
//      *textureM = pointer to TextureManager object
//      *healthTexture = pointer to health's TextureManager object
// Post: returns true if successful, false if failed
//=============================================================================
bool GeneralEntity::initialize(Game *gamePtr, TextureManager *textureM, TextureManager *healthTexture)
{
	if (!healthImage.initialize(gamePtr->getGraphics(), GENERAL_WIDTH, GENERAL_HEIGHT, healthTexture->getWidth()/GENERAL_WIDTH, healthTexture)){
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init health"));
		return false;
	}
	healthImage.setScale(GENERAL_IMAGE_SCALE);
	healthImage.setCurrentFrame(health);
	return(Entity::initialize(gamePtr, GENERAL_WIDTH, GENERAL_HEIGHT, textureM->getWidth()/GENERAL_WIDTH, textureM));
}

void GeneralEntity::doInvincible(){
}

void GeneralEntity::walk(){
	setFrames(GENERAL_WALKING_START*GENERAL_NUM_DIRECTIONS+direc, GENERAL_WALKING_END*GENERAL_NUM_DIRECTIONS+direc);
}

void GeneralEntity::attack(){
	setFrames(GENERAL_ATTACKING*GENERAL_NUM_DIRECTIONS+direc, GENERAL_ATTACKING*GENERAL_NUM_DIRECTIONS+direc);
	setCurrentFrame(GENERAL_ATTACKING*GENERAL_NUM_DIRECTIONS+direc);
}

void GeneralEntity::rest(){
	setCurrentFrame(GENERAL_RESTING_START*GENERAL_NUM_DIRECTIONS+direc);
	setFrames(GENERAL_RESTING_START*GENERAL_NUM_DIRECTIONS+direc, GENERAL_RESTING_END*GENERAL_NUM_DIRECTIONS+direc);
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void GeneralEntity::update(float frameTime)
{
	healthImage.setCurrentFrame(health);
	healthImage.setX(spriteData.x);
	healthImage.setY(spriteData.y);
	healthImage.update(frameTime);
	//Entity::update(frameTime);

	if(invincible){
		doInvincible();
	}
	else if((velocity.x != 0 || velocity.y != 0) && !attacking){
		walk();
	}
	else if(attacking){
		attack();
	}
	else{
		rest();
	}

	if (endFrame - startFrame > 0)          // if animated sprite
	{
		animTimer += frameTime;             // total elapsed time
		if (animTimer > frameDelay)
		{
			animTimer -= frameDelay;
			currentFrame += GENERAL_NUM_DIRECTIONS;
			if (currentFrame < startFrame || currentFrame > endFrame)
			{
				if(loop == true)            // if looping animation
					currentFrame = startFrame;
				else                        // not looping animation
				{
					currentFrame = endFrame;
					animComplete = true;    // animation complete
				}
			}
			setRect();                      // set spriteData.rect
		}
	}
}

//=============================================================================
// Draw the image using the color filter
// Pre : spriteBegin() is called
// Post: spriteEnd() is called
//=============================================================================
void GeneralEntity::draw(void){
	if(health > 0){
		healthImage.draw();
	}
	Entity::draw(spriteData, colorFilter);
}

//=============================================================================
// Perform collision detection between this entity and the other Entity.
// Each entity must use a single collision type. Complex shapes that require
// multiple collision types may be done by treating each part as a separate
// entity.
// Typically called once per frame.
// The collision types: CIRCLE, BOX, or ROTATED_BOX.
// Post: returns true if collision, false otherwise
//       sets collisionVector if collision
//=============================================================================
bool GeneralEntity::collidesWith(GeneralEntity &ent, VECTOR2 &collisionVector){
	if(invincible || ent.isInvincible()){
		return false;
	}
	return Entity::collidesWith(ent, collisionVector);
}
bool GeneralEntity::collidesWith(Entity &ent, VECTOR2 &collisionVector){
	if(invincible){
		return false;
	}
	return Entity::collidesWith(ent, collisionVector);
}