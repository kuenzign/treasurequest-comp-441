// Programming 2D Games
// Copyright (c) 2011 by:
// Charles Kelly
// Parallax Scroll constants.h v1.1
// Last modification: Feb-11-2013

#ifndef _CONSTANTS_H // Prevent multiple definitions if this
#define _CONSTANTS_H // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//=============================================================================
// Function templates for safely dealing with pointer referenced items.
// The functions defined by these templates may be called using a normal
// function call syntax. The compiler will create a function that replaces T
// with the type of the calling parameter.
//=============================================================================
// Safely release pointer referenced item
template <typename T>
inline void safeRelease(T &ptr)
{
	if (ptr)
	{
		ptr->Release();
		ptr = NULL;
	}
}
#define SAFE_RELEASE safeRelease // for backward compatiblility

// Safely delete pointer referenced item
template <typename T>
inline void safeDelete(T &ptr)
{
	if (ptr)
	{
		delete ptr;
		ptr = NULL;
	}
}
#define SAFE_DELETE safeDelete // for backward compatiblility

// Safely delete pointer referenced array
template <typename T>
inline void safeDeleteArray(T &ptr)
{
	if (ptr)
	{
		delete[] ptr;
		ptr = NULL;
	}
}
#define SAFE_DELETE_ARRAY safeDeleteArray // for backward compatiblility

// Safely call onLostDevice
template <typename T>
inline void safeOnLostDevice(T &ptr)
{
	if (ptr)
		ptr->onLostDevice();
}
#define SAFE_ON_LOST_DEVICE safeOnLostDevice // for backward compatiblility

// Safely call onResetDevice
template <typename T>
inline void safeOnResetDevice(T &ptr)
{
	if (ptr)
		ptr->onResetDevice();
}
#define SAFE_ON_RESET_DEVICE safeOnResetDevice // for backward compatiblility

#define RAND_FLOAT(low, high) (low + (float)rand() / ((float)RAND_MAX + 1) * (high - low))

//=============================================================================
//                  Constants
//=============================================================================

// window
const char CLASS_NAME[] = "TreasureQuest";
const char GAME_TITLE[] = "TreasureQuest";
const bool FULLSCREEN = false; // windowed or fullscreen
const UINT GAME_WIDTH = 1284;  // width of game in pixels
const UINT GAME_HEIGHT = 640;  // height of game in pixels

// game
const double PI = 3.14159265;
const float FRAME_RATE = 400.0;			    // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;		    // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f / FRAME_RATE;     // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f / MIN_FRAME_RATE; // maximum time used in calculations

// graphic images
const char MENU_IMAGE[] = "pictures\\menu.png"; // menu texture
const char HEALTH_IMAGE[] = "pictures\\Nathan\\Health\\compiled.png";
const char MAP1_IMAGE[] = "pictures\\Nathan\\grass.png";
const char MAP1_SEL_IMAGE[] = "pictures\\Nathan\\grassSel.png";
const char MAP2_IMAGE[] = "pictures\\Nathan\\dirt.png";
const char MAP2_SEL_IMAGE[] = "pictures\\Nathan\\dirtSel.png";
const char MAP3_IMAGE[] = "pictures\\Nathan\\stone.png";
const char MAP3_SEL_IMAGE[] = "pictures\\Nathan\\stoneSel.png";
const char ARCHER_IMAGE[] = "pictures\\Nathan\\Archer\\compiled.png";
const float ARROW_IMAGE_SCALE = 1.0f;
const char SKELETON_IMAGE[] = "pictures\\Nathan\\Skeleton\\compiled.png";
const char ORC_IMAGE[] = "pictures\\Nathan\\Orc\\compiled.png";
const char SLIME_IMAGE[] = "pictures\\Nathan\\Slime\\compiled.png";
const char LOGO_IMAGE[] = "pictures\\Nathan\\logo.png";
const float LOGO_IMAGE_SCALE = 0.5f;
const char CHARACTER_BOX_IMAGE[] = "pictures\\BoxSelect.png";
const float CHARACTER_BOX_IMAGE_SCALE = 0.8f;
const char CHARACTER_BACKGROUND_IMAGE[] = "pictures\\BlankPanel-1.png";
const float CHARACTER_BACKGROUND_SCALE = 0.8f;
const char ARCHER_BACKGROUND_IMAGE[] = "pictures\\Nathan\\Archer\\e.png";
const char WIZARD_BACKGROUND_IMAGE[] = "pictures\\Nathan\\Wizard\\wizard.png";
const char SWORDSMAN_BACKGROUND_IMAGE[] = "pictures\\Nathan\\Swordsman\\swordsman.png";
const float CHARACTER_SELECT_SCALE = 0.5f;
const char SMALL_TREES[] = "pictures\\pine_shadow.png";
const float SMALL_TREES_SCALE = 1.5f;
const char SNOW_TREES[] = "pictures\\pinesnow.png";
const float SNOW_TREES_SCALE = 1.1f;
const char DUST_IMAGE[] = "pictures\\blood.png";

const float MAP1_IMAGE_SCALE = 0.5f;
const float MAP2_IMAGE_SCALE = 0.5f;
const float MAP3_IMAGE_SCALE = 0.5f;
const int MAP_IMAGE_ROWS = 8;
const int MAP_IMAGE_COLUMNS = 8;

// audio files required by audio.cpp
// WAVE_BANK must be location of .xwb file.
const char WAVE_BANK[] = "audio\\Win\\Wave Bank.xwb";
// SOUND_BANK must be location of .xsb file.
const char SOUND_BANK[] = "audio\\Win\\Sound Bank.xsb";

//Song for the menus
const char MENU_SONG[] = "adventure-fantasy";
const char GAME_SONG[] = "level1";
const char SHOT_SONG[] = "regular-arrow-shot";
const char CRY_SONG[] = "battlecry";
const char SLIME_DOWN[] = "slime-down";
const char SLIME_UP[] = "slime-up";
const char SWORD_THROW[] = "sword-throw";
const char ORC_BITE[] = "orc-bite";

// audio cues
const char ENGINE1[] = "engine1";

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR CONSOLE_KEY = '`';     // ` key
const UCHAR ESC_KEY = VK_ESCAPE;   // escape key
const UCHAR ALT_KEY = VK_MENU;     // Alt key
const UCHAR ENTER_KEY = VK_RETURN; // Enter key

//Menu stuff
const char CASTLE_1_IMAGE[] = "pictures\\Castle.png";
const char CASTLE_2_IMAGE[] = "pictures\\Castle2.png";
const char ARROW_IMAGE[] = "pictures\\Luis\\myArrow.png";
const char ALT_MENU[] = "pictures\\background_resized.png";
const char MENU_BOX[] = "pictures\\box_resized.png";

enum GameStates
{
	menu,
	tutorial,
	resetstuff,
	characterselect,
	prep,
	level1,
	l1complete,
	level2,
	level3,
	l2complete,
	lose,
	end
};
enum direction
{
	upright,
	downright,
	downleft,
	upleft,
	none
};

// Pattern Step Action
enum PATTERN_STEP_ACTION
{
	NONE,
	UP,
	DOWN,
	LEFT,
	topLEFT,
	RIGHT,
	TRACK,
	EVADE,
	DIAGONAL,
	DIAGINV,
	DELTATRACK
};

const float MAX_PARTICLE_LIFETIME = 1.5f;
const int MAX_NUMBER_PARTICLES = 500;

const int GENERAL_COLLISION_WIDTH = 242;
const int GENERAL_COLLISION_HEIGHT = 300;

const int GENERAL_NUM_DIRECTIONS = 4;
const int GENERAL_TILE_WIDTH = 450;
const int GENERAL_TILE_HEIGHT = 260;
const int GENERAL_WIDTH = 450;
const int GENERAL_HEIGHT = 520;
const float GENERAL_IMAGE_SCALE = 0.35f;
const float GENERAL_ANIMATION_DELAY = 0.12f;
const int GENERAL_RESTING_START = 0;
const int GENERAL_RESTING_END = 0;
const int GENERAL_WALKING_START = 0;
const int GENERAL_WALKING_END = 1;
const int GENERAL_ATTACKING = 2;

const int ARCHER_SHOOTING_START = 2;
const int ARCHER_SHOOTING_END = 4;
const int SLIME_INVINCIBLE_START = 3;
const int SLIME_INVINCIBLE_END = 4;

const float SLIME_ANIMATION_DELAY = 0.24f;

inline void convertScreenToGrid(float &x, float &y)
{
	float mapWidth = GAME_WIDTH / MAP_IMAGE_COLUMNS;
	float mapHeight = GAME_HEIGHT / MAP_IMAGE_ROWS;
	float x1 = x - ((int)(x - (mapWidth / 2)) % (int)(mapWidth / 2));
	float x2 = x1 + (mapWidth / 2);
	float y1 = y - ((int)(y - (mapHeight / 2)) % (int)(mapHeight / 2));
	float y2 = y1 + (mapHeight / 2);
	if ((int)x1 % (int)mapWidth < mapWidth / 4 && (int)y1 % (int)mapHeight > mapHeight / 4)
	{
		y1 += mapHeight / 2;
		y2 -= mapHeight / 2;
	}
	else if ((int)x1 % (int)mapWidth > mapWidth / 4 && (int)y1 % (int)mapHeight < mapHeight / 4)
	{
		x1 += mapWidth / 2;
		x2 -= mapWidth / 2;
	}
	float stretchY1 = ((y1 - (mapHeight / 2)) / (mapHeight / 2)) * mapHeight;
	float stretchY2 = ((y2 - (mapHeight / 2)) / (mapHeight / 2)) * mapHeight;
	float stretchY = ((y - (mapHeight / 2)) / (mapHeight / 2)) * mapHeight;
	float diff1 = ((x - x1) * (x - x1)) + ((stretchY - stretchY1) * (stretchY - stretchY1));
	float diff2 = ((x2 - x) * (x2 - x)) + ((stretchY2 - stretchY) * (stretchY2 - stretchY));
	if (diff1 < diff2)
	{
		x = x1;
		y = y1;
	}
	else
	{
		x = x2;
		y = y2;
	}
}
#define CONVERT_SCREEN_TO_GRID convertScreenToGrid

#endif
